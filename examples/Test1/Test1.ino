/*
 * Demonstrate pin change interrupts
 * 
 * Uses the Southeastcon 2017 robot contest arena Controller, its two
 * pushbuttons, and an additional button on the relay board tester (pin 2).
 * 
 * Display message on different display lines corresponding to button pushes.
 * Switch bounce and race conditions are ignored but mostly harmless.
 *
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 Peter James Soper pete@soper.us
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <Arduino.h>
#include <Wire.h> 
#include <Sainsmart_I2CLCD.h>
#include <SimplePinChange.h>

Sainsmart_I2CLCD lcd(0x27,20,4);  // I2C address 0x27, four lines of 20 chars

// Mapping of pin numbers to switch positions

volatile uint8_t pins[] = {14, 2, 16, 17};
// Current switch state
volatile uint8_t s[sizeof(pins)];

// Scan pins and set current switch state
void switch_press() {
  for (uint8_t i = 0; i < sizeof(s); i++) {
    s[i] = ! digitalRead(pins[i]);
  }
}

// Put out a message on line 1-4
void message(int line, const char msg[], bool show_pin = true) {
  lcd.setCursor(0,line - 1);
  lcd.print(msg);
  if (show_pin) {
    lcd.print(" ");
    lcd.print(pins[line-1]);
  }
}

// Initialize pin modes, change interrupts, and LCD display

void setup() {
  for (uint8_t i = 0; i < sizeof(s); i++) {
    pinMode(pins[i], INPUT_PULLUP);
    SimplePinChange.attach(pins[i], switch_press);
  }
  lcd.init();
  lcd.backlight();
  lcd.home();
  message(1, "Press a controller", false);
  message(2, "or relay tester", false);
  message(3, "'SW' button", false);
}


// Look for changes in the switch states and display messages

void loop() {

  static bool initial_clear_needed = true;
  bool anyseen = false;

  for (uint8_t i = 0; i < sizeof(s); i++) {
    if (s[i]) {
      if (initial_clear_needed) {
        lcd.clear();
        initial_clear_needed = false;
      }
      message(i+1,"Press");
      // If the switch is already released this is harmless
      s[i] = 0;
      anyseen = true;
    }
  }

  if (anyseen) {
    delay(1000);
    lcd.clear();
  } 

  // This delay makes it possible to test multiple presses
  delay(50);
}
