/*
 * Test the pin change library.
 * Blink the LED at a varying rate depending on which input pin 
 * has been pulled to ground.
 * But much more involved than Test2.
 * Initially set up the relay tester board's pin 2 switch
 * first. Then, when that's been pressed once, attach an interrput for the
 * arena controller switch on A0. When the pin 2 switch is pressed again, 
 * attach the A3 switch of the controller. When 2 is pressed again, detach
 * the A3 switch. Finally, when 2 is pressed again, detach the A0 switch.
 * Then the whole thing can be repeated. Point is to confirm that multiple
 * pins of a port (i.e. the one controlling both A0 and A3) can be handled
 * correctly. An unexpected return value causes a error message to the serial
 * output (9600 baud).
 * Explicit tests to force and test attach/detach return values are also done.
 * Switch presses are debounced and a second press within 25-50 milliseconds of
 * the first is ignored.
 *
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 Peter James Soper pete@soper.us
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <MsTimer2.h>
#include <SimplePinChange.h>

#define LED 4

// This is on one port
#define CONTROL_BUTTON 2

// Both of these are on a different pot
#define OTHER_BUTTON0 A0
#define OTHER_BUTTON1 A3

static volatile long delay_time = 50;

// pins A0 or A3
// Intent is that alternating A0, A3 switches will allow confirming the
// detection of each in turn by alternating delays.
void int1() {
  if(delay_time == 500) {
    delay_time = 250;
  } else {
    delay_time = 500;
  }
}

#define NUM_STATES 5

volatile static uint8_t sw_pressed = false;
volatile int sw_pressed_count = 0;

long sw_ticks = 0;
long ticks = 1;

// pin 2
void int2() {
  delay_time = 1000;
  SimplePinChange.detach(CONTROL_BUTTON);
  // This scheme will result in a spurious button press every six years or so
  sw_ticks = ticks + 2;
}

// periodic interrupt
void tick() {
  ticks += 1;
  if (ticks == sw_ticks) {
    sw_pressed_count += 1;
    sw_pressed = true;
  }
}

void setup() {
  Serial.begin(9600);
  MsTimer2::set(25, tick);
  MsTimer2::start();
  if (SimplePinChange.attach(-1,int2)) {
    Serial.println("first bad pin # attach returned surprise true");
  }
  if (SimplePinChange.attach(-1,int2)) {
    Serial.println("second bad pin # attach returned surprise true");
  }
  if (SimplePinChange.attach(22,int2)) {
    Serial.println("third bad pin # attach returned surprise true");
  }
  if (SimplePinChange.attach(22,int2)) {
    Serial.println("fourth bad pin # attach returned surprise true");
  }
  if (SimplePinChange.attach(2,int2)) {
    Serial.println("fifth attach returned surprise true");
  }
  if (!SimplePinChange.attach(3,int2)) {
    Serial.println("sixth attach returned surprise false");
  }
  if (SimplePinChange.detach(3)) {
    Serial.println("first detach returned surprise true");
  }
  if (!SimplePinChange.detach(2)) {
    Serial.println("second detach returned surprise false");
  }
  if (SimplePinChange.detach(-1)) {
    Serial.println("third detach with bad pin returned surprise false");
  }
  if (SimplePinChange.detach(22)) {
    Serial.println("fourth detach with bad pin returned surprise false");
  }
  pinMode(LED,OUTPUT);
  pinMode(CONTROL_BUTTON,INPUT_PULLUP);
  SimplePinChange.attach(CONTROL_BUTTON,int2);
  pinMode(OTHER_BUTTON0,INPUT_PULLUP);
  pinMode(OTHER_BUTTON1,INPUT_PULLUP);
}

void loop() {
  static uint8_t control_state = 0;
  static byte led_state = 0;
  digitalWrite(LED,led_state = ! led_state);
  delay(delay_time);
  if (sw_pressed) {
    Serial.print("press seen ");
    cli();
    int temp = sw_pressed_count;
    sei();
    Serial.println(temp);
    delay(25);
    sw_pressed = false;
    SimplePinChange.attach(CONTROL_BUTTON,int2);
    Serial.print("State: ");
    Serial.println(control_state);
    switch (control_state) {
      case 0: if (SimplePinChange.attach(A0, int1)) {
		Serial.println("attach with state 0 returned surprise true");
	      }
	      control_state = 1;
	      break;
      case 1: if (!SimplePinChange.attach(A3, int1)) {
		Serial.println("attach with state 1 returned surprise false");
	      }
	      control_state = 2;
	      break;
      case 2: if (SimplePinChange.detach(A3)) {
		Serial.println("detach with state 2 returned surprise true");
	      }
	      control_state = 3;
	      break;
      case 3: if (!SimplePinChange.detach(A0)) {
		Serial.println("detach with state 3 returned surprise false");
	      }
	      control_state = 0;
	      break;
      default:
	      Serial.println("state switch statement screwed up");
    }
  }
}

