/*
 * Test the pin change library.
 *
 * Blink the LED at a varying rate depending on which input pin 
 * has been pulled to ground by one of three switches.
 *
 * This code is simple. It does nothing about switch bounces (because this
 * app doesn't need to). But it also ignores the fact that an interrupt could 
 * fiddle with parts of the long variable 'delay_time' while the code in the 
 * loop() function is trying to use the value. That is, while the variable is
 * being pushed on the hardware stack and before the delay() function is called,
 * some "middle bits" of the variable can get flipped. This could manifest
 * as additional delay values for a single delay. For this application this
 * kind of improper handling is harmless, but for others it could cause 
 * serious misbehavior. Access to the variable would ordinarily be "guarded"
 * in such a way that it couldn't be modified by interrupt code until after
 * it was completely accessed.
 *
 * Change Log
 * April, 2017 - original
 * August, 2017 - added a few comments, cleaned up coding style
 *
 * The MIT License (MIT) (see "LICENSE")
 */

#include <SimplePinChange.h>

/* Which Arduino pin the LED is connected to. The connection should be pin 4
 * to the LED anode, then a current limiting resistor from the LED cathode to
 * ground. A 470 ohm resistor should work for any color LED.
 */

#define LED 4

/* The PinChange interrupt library easily supports three pins: one of eight
 * pins making up Atmega328p "port B", one from "port C", and one from "port D".
 * The three pin assignments below fall within the range of separate ports:
 * one per button. Each port of an Arduino chip has up to eight pins defined
 * for it.
 * More details can be seen with this diagram of the Atmega328p chip that's in
 * an Arduino Uno:
 * https://upload.wikimedia.org/wikipedia/commons/c/c9/Pinout_of_ARDUINO_Board_and_ATMega328PU.svg
 * So, from the diagram it can be seen that pins 0-7 belong to port D,
 * A0-A5 belong to port C, and 8-13 belong to port B.
 *
 * So, with the example below BUTTON0 causes a pin change interrupt for
 * port B, BUTTON1 A0 for port C, and BUTTON2 for port D.
 *
 * (Advanced topic: a better version of the pin change library could 
 *  differentiate between multiple pins of a port with change interrupts,
 *  but only for limited cases: the general case is always ambiguous.)
 */

#define BUTTON0 9
#define BUTTON1 A0
#define BUTTON2 2

// Initial milliseconds to sleep in loop()

static volatile long delay_time = 50;

// Handle the interrupt caused by a change of the input to a port B pin

/*
 * Switch bouncing and release of a switch just causes additional
 * interrupts and redundantly sets the delay time. In MANY other
 * applications this would not be sufficient!
 */

void int0() {
  delay_time = 100;
}

// Handle the interrupt caused by a change of the input to a port C pin

void int1() {
  delay_time = 500;
}

// Handle the interrupt caused by a change of the input to a port D pin

void int2() {
  delay_time = 2000;
}

// This function called once by the Arduino runtime

void setup() {
  pinMode(LED, OUTPUT);

  // Each button's pin is set to "input with pullup" so the button can
  // simply connect the pin to ground to trigger an interrupt

  pinMode(BUTTON0, INPUT_PULLUP);
  SimplePinChange.attach(BUTTON0, int0);

  pinMode(BUTTON1,INPUT_PULLUP);
  SimplePinChange.attach(BUTTON1, int1);

  pinMode(BUTTON2,INPUT_PULLUP);
  SimplePinChange.attach(BUTTON2, int2);
}

// This function called over and over by the Arduino runtime

void loop() {
  // Initialize the LED state once. 
  static byte state = 0;

  // Invert the output to the LED
  digitalWrite(LED, state = ! state);

  // Wait for the programmed number of milliseconds
  delay(delay_time);
}

