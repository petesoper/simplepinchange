/*
 * Exercise Tom Rauschenbach's display board with switch presses detected
 * with pin change interrupts.
 *
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 Peter James Soper pete@soper.us
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <SimplePinChange.h>
#include <MsTimer2.h>

const int Rclk = 5;
const int Sclk = 6;
const int SERPIN = 7;

static volatile uint16_t c_0, c_1, c_2, c_3;
static volatile uint16_t r_0, r_1, r_2, r_3;

static unsigned long start;

static const char digiTab[] = { 0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D,
                    0x07, 0x7F, 0x6F, 0x77, 0x7C, 0x39, 0x5E, 0x79, 0x71 };

static volatile uint8_t *rclk_p;
static uint8_t rclk_b;
static volatile uint8_t *sclk_p;
static uint8_t sclk_b;
static volatile uint8_t *ser_p;
static uint8_t ser_b;

static void rOff() {
  uint8_t oldSREG = SREG;
  cli();
  *rclk_p &= ~rclk_b;
  SREG = oldSREG;
}

static void rOn() {
  uint8_t oldSREG = SREG;
  cli();
  *rclk_p |= rclk_b;
  SREG = oldSREG;
}

static void send(uint8_t val) {
  uint8_t mask = 0x80;
  uint8_t oldSREG = SREG;
  cli();
  for (int i=0;i<8;i++) {
    *sclk_p &= ~sclk_b;
    *ser_p = val & mask ? *ser_p | ser_b : *ser_p & ~ser_b;
    *sclk_p |= sclk_b;
    mask >>= 1;
  }
  SREG = oldSREG;
}

void int0() {
  if (digitalRead(2)) {
    c_0 += 1;
  } else {
    c_1 += 1;
  }
}

void displayValue(unsigned int val, uint8_t aux) {
  unsigned int div = 10000;
  rOff();
  send(aux);
  uint8_t seen=0;
  for (int i=1;i<=5;i++) {
    uint8_t t = val / div;
    if (t == 0) {
      if (seen) {
	t = digiTab[0];
      } else {
	t = 0;
      }
    } else {
      seen = 1;
      t = digiTab[t];
    }
      
    send(t);
    val %= div;
    div /= 10;
  }
  rOn();
}

void tick() {
  r_0 = c_0;
  r_1 = c_1;
  r_2 = c_2;
  r_3 = c_3;
  c_0 = c_1 = c_2 = c_3 = 0;
}

void setup() {
  pinMode(SERPIN, OUTPUT);
  pinMode(Rclk, OUTPUT);
  pinMode(Sclk, OUTPUT);
  rclk_p = portOutputRegister(digitalPinToPort(Rclk));
  rclk_b = digitalPinToBitMask(Rclk);
  sclk_p = portOutputRegister(digitalPinToPort(Sclk));
  sclk_b = digitalPinToBitMask(Sclk);
  ser_p = portOutputRegister(digitalPinToPort(SERPIN));
  ser_b = digitalPinToBitMask(SERPIN);
  MsTimer2::set(1000, tick);
  MsTimer2::start();
  SimplePinChange.attach(2,int0);
  //attachInterrupt(0, int0, RISING);
  start = micros();
  Serial.begin(9600);
}

void loop() {
  static uint8_t state = 0;
  volatile uint16_t v0, v1, v2, v3;
  delay(1000);
  uint8_t oldSREG = SREG;
  cli();
  v0 = r_0;
  v1 = r_1;
  v2 = r_2;
  v3 = r_3;
  SREG = oldSREG;
  switch (state) {
    case 0:
      displayValue(v0, 0x80);
      break;
    case 1:
      displayValue(v1, 0x40);
      break;
    case 2:
      displayValue(v2, 0x20);
      break;
    case 3:
      displayValue(v3, 0x10);
      break;
  }
  state = (state + 1) % 4;
}
